<?php

class TasksController {

    private $tasksManager;
    private $userManager;

    public function __construct($tasksModel, $userModel) {
        $this->tasksManager = $tasksModel;
        $this->userManager = $userModel;
    }

    public function indexAction($request) {
        $tasks = $this->tasksManager->findAllTasks();
        $users = $this->userManager->findAll();
        $View = new TasksView($this->tasksManager);

        $data = [
            'tasks' => $tasks,
            'isAdmin' => $this->userManager->isAdmin(),
            'users' => $users
        ];
        $View->renderView($data);
    }

    public function getAction($request) {
        $tasks = $this->tasksManager->findAllTasks();
        $data = '';
        $isAdmin = $this->userManager->isAdmin();
        foreach ($tasks as $task) {
            if ($task['status'] == 1) {
                $sflag = ' style="background-color: #78da786b;"';
            } else {
                $sflag = '';
            }
            if ($task['dstatus'] == 1) {
                $dflag = '<p class="adm-edited">Отред. администратором</p>';
            } else {
                $dflag = '';
            }
            $data .= '<tr data-ed="task" data-target="'.$task['id'].'"'.$sflag.'>';
            $data .= '<td>'.$task['id'].'</td>';
            $data .= '<td>'.$task['fio'].'</td>';
            $data .= '<td>'.$task['email'].'</td>';
            if ($isAdmin && $task['status'] == 0) {
                $data .= '<td>'.$dflag.'<span>'.$task['content'].'</span><button title="Изменить описание задачи" data-ed="adm-action" data-upd="content" data-toggle="modal" data-target="#taskEditModal" data-tid="'.$task['id'].'">Ред.</button></td>';
                $data .= '<td><span>'.$task['status_text'].'</span><button title="Отметить как выполненую" data-ed="adm-action" data-upd="status" data-tid="'.$task['id'].'">Вып.</button></td>';
            } else {
                $data .= '<td>'.$dflag.'<span>'.$task['content'].'</span></td>';
                $data .= '<td><span>'.$task['status_text'].'</span></td>';
            }
            
            $data .= '</tr>';
        }

        echo $data;
    }
    public function editAction($request) {
        if ($this->userManager->is_logined()) {
            if (!isset($request['desc'])) {
                if ($this->tasksManager->updateStatus($request['taskId'])) {
                    echo json_encode([
                        'status' => 1
                    ]);
                } else {
                    echo json_encode([
                        'status' => 0
                    ]);
                }
            } else {
                if ($this->tasksManager->updateDesc($request['taskId'], $request['desc'])) {
                    echo json_encode([
                        'status' => 1
                    ]);
                } else {
                    echo json_encode([
                        'status' => 0
                    ]);
                }
            }
        } else {
            echo json_encode([
                'status' => -1
            ]);
        }
    }
    
    public function addAction($request) {
        //$data = $this->tasksManager->addTask($request['id'], $request['desc']);
        //$res = $this->userManager->getUser($request['id']);
        $res = $this->userManager->getUser($request['name'], $request['email']);
        $data = $this->tasksManager->addTask($res['id'], $request['desc']);

        if ($data && $res) {
            echo json_encode(array(
                'tid' => $data['id'],
                'desc' => $data['content'],
                'status' => 1,
                'id' => $res['id'],
                'email' => $res['email'],
                'login' => $res['login'],
                'fio' => $res['fio']
            ));
        } else {
            echo json_encode(array(
                'status' => 0
            ));
        }
    }

    public function redirectAction($route="/") {
        header("location: $route");
        exit;
    }

}