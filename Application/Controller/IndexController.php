<?php

class IndexController {

    private $tasksManager;
    private $userManager;

    public function __construct($tasksModel, $userModel) {
        $this->tasksManager = $tasksModel;
        $this->userManager = $userModel;
    }

    public function indexAction($request) {
        $View = new TasksView($this->tasksManager);
        $View->indexView();
    }

    public function aboutAction() {
        $View = new TasksView($this->tasksManager);
        $View->renderView();
    }

    public function loginAction() {
        if ($this->userManager->is_logined()) {
            $this->redirectAction();
        }
        $View = new TasksView($this->tasksManager);
        $View->renderView();
    }

    public function loginsubmittedAction($request) {
        if ($this->userManager->login($request['login'], $request['password'])) {
            $this->redirectAction();
            unset($_COOKIE['auth_error']);
        } else {
            setcookie("auth_error", 1, time()+5);
            $this->redirectAction("/?action=login&error=error");
        }
    }

    public function logoutAction() {
        $this->userManager->logout();
        $this->redirectAction();
    }
    
    public function redirectAction($route="/") {
        header("location: $route");
        exit;
    }

}
