<?php

class UserManager {

    private $db;

    public function __construct($appConfig = null) {
        session_start();
        try {
            $c_tmp = $appConfig['connection']['params'];
            $this->db = new PDO ('mysql:host=' . $c_tmp['host'] . ';dbname=' . $c_tmp['dbname'], $c_tmp['user'], $c_tmp['password'] );
            $this->db->query('SET NAMES utf8;');
        } catch (Exception $e) {
            die("Ошибка подключения к базе данных.");
        }
    }

    private function edBycicle($str) {
        $str = trim($str);
        $str = stripslashes($str);
        $str = htmlspecialchars($str);
        return $str;
    }
    public function findUser($login, $password) {
        if ($res = $this->db->query("SELECT * FROM bj_users WHERE login = '".$this->edBycicle($login)."' AND password = '".md5($this->edBycicle($password))."';")) {
            if ($res->rowCount() > 0) {
                $res = $res->fetch();
                return array(
                    'id' => $res['id'],
                    'email' => $res['email'],
                    'login' => $res['login'],
                    'fio' => $res['fio']
                );
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
    public function getUser($name, $email) {
        $res = $this->db->query("SELECT * FROM bj_users WHERE fio = '".$this->edBycicle($name)."' AND email = '".$this->edBycicle($email)."';");
        if ($res && $res->rowCount() > 0) {
            return $res->fetch();
        } else {
            $res = $this->db->query("INSERT INTO bj_users (login, email, fio, password, status) VALUES ('test', '".$this->edBycicle($email)."', '".$this->edBycicle($name)."', 'test', '0');");
            if ($res->rowCount()) {
                return $this->db->query("SELECT * FROM bj_users ORDER BY id DESC LIMIT 1")->fetch();
            } else {
                return false;
            }
        }
    }
    public function findAll() {
        $users = [];
        $data = $this->db->query("SELECT * FROM bj_users;")->fetchAll();
        foreach ($data as $res) {
            $users[] = [
                'id' => $res['id'],
                'email' => $res['email'],
                'login' => $res['login'],
                'fio' => $res['fio']
            ];
        }
        return $users;
    }

    public function login($login, $password) {

        $user = $this->findUser($login, $password);

        if ($user) {
            $_SESSION['adminid'] = $user['id'];
            $_SESSION['adminlogin'] = $user['fio'];
            return true;
        }
        return false;
    }

    public function logout() {
        unset($_SESSION['adminid']);
        return true;
    }

    public function is_logined() {
        if (isset($_SESSION['adminid']) && $_SESSION['adminid']) {
            return $_SESSION['adminid'];
        } else {
            return false;
        }
    }

    public function isAdmin() {
        if (isset($_SESSION['adminid']) && $_SESSION['adminid'] && $_SESSION['adminid'] == 4) {
            return true;
        } else {
            return false;
        }
    }

}