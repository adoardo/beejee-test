<?php

class TasksManager {

    private $db;

    public function __construct($appConfig = null) {
        try {
            $c_tmp = $appConfig['connection']['params'];
            $this->db = new PDO ('mysql:host=' . $c_tmp['host'] . ';dbname=' . $c_tmp['dbname'], $c_tmp['user'], $c_tmp['password'] );
            $this->db->query('SET NAMES utf8;');
        } catch (Exception $e) {
            die("Ошибка подключения к базе данных.");
        }
    }

    private function edBycicle($str) {
        $str = trim($str);
        $str = stripslashes($str);
        $str = htmlspecialchars($str);
        return $str;
    }
    public function updateStatus($id) {
        return $this->db->query('UPDATE bj_tasks SET status = 1 WHERE id = '.$this->edBycicle($id).';');
    }
    public function updateDesc($id, $desc) {
        return $this->db->query('UPDATE bj_tasks SET dstatus = 1, content = "'.$this->edBycicle($desc).'" WHERE id = '.$this->edBycicle($id).';');
    }
    public function addTask($userid, $desc) {
        $res = $this->db->query("INSERT INTO bj_tasks (user_id, content, status, dstatus) VALUES ('".$this->edBycicle($userid)."', '".$this->edBycicle($desc)."', 0, 0);");
        if ($res->rowCount()) {
            return $this->db->query("SELECT * FROM bj_tasks ORDER BY id DESC LIMIT 1")->fetch();
        } else {
            return false;
        }
    }
    public function findAllTasks() {
        $tasks = array();
        $res = $this->db->query('SELECT * FROM bj_tasks ORDER BY created_at ASC;');
        while ($task = $res->fetch()) {
            $user = $this->db->query('SELECT * FROM bj_users WHERE id = '.$task['user_id'].';');
            if ($user->rowCount() > 0) {
                $user = $user->fetch();
                if ($task['status'] == 1) {
                    $status = 'Выполнено';
                } else {
                    $status = 'В работе';
                }
                $tasks[] = [
                    'id' => $task['id'],
                    'fio' => $user['fio'],
                    'email' => $user['email'],
                    'content' => $task['content'],
                    'status' => $task['status'],
                    'dstatus' => $task['dstatus'],
                    'status_text' => $status
                ];
            }
        }
        return $tasks;
    }

}
