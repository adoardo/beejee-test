<?php
ini_set('display_errors', 1);
error_reporting(E_ALL);

/**
 * Конфиг БД
 */
$appConfig = require __DIR__ . '/../config/application.config.php';

/**
 * Роутер
 */
if (isset($_REQUEST['action'])) {

    switch ($_REQUEST['action']) {
        case 'about':
            $controller_name = 'IndexController';
            $action = 'aboutAction';
            break;
        
        case 'login':
            $controller_name = 'IndexController';
            $action = 'loginAction';
            break;
        
        case 'loginsubmitted':
            $controller_name = 'IndexController';
            $action = 'loginsubmittedAction';
            break;
        
        case 'logout':
            $controller_name = 'IndexController';
            $action = 'logoutAction';
            break;

        case 'edit':
            $controller_name = 'TasksController';
            $action = 'editAction';
            break;

        case 'add':
            $controller_name = 'TasksController';
            $action = 'addAction';
            break;

        case 'get':
            $controller_name = 'TasksController';
            $action = 'getAction';
            break;

    }
} else {
    $controller_name = 'TasksController';
    $action = 'indexAction';
}

/**
 * Контроллер
 */
require '../Application/Controller/' . $controller_name . '.php';

/**
 * Подключаем авторизацию и таски
 */
require '../Application/Model/TasksManager.php';
require '../Application/Model/UserManager.php';
$tasksManager = new TasksManager($appConfig);
$userManager = new UserManager($appConfig);

/**
 * Визуализируем
 */
require '../Application/View/Init.php';
$controller = new $controller_name($tasksManager, $userManager);
$controller->{$action}($_REQUEST);

