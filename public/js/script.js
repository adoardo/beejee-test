document.addEventListener("DOMContentLoaded", function() {

    window.table = $('#ed-task-table');

    //$('#task-user').select2();

    function getText($sel) {
        return $sel.clone()
            .children()
            .remove()
            .end()
            .text()
            .trim();
    }

    function buildTable(dest) {
        if (dest == 1) {
            window.table.DataTable().destroy();
        }
        $.ajax({
                type: "POST",
                url: '/?action=get',
                success: function(response) {
                    window.tableData = response;
                    window.table.find('tbody').html(window.tableData);
                    window.table.DataTable({
                        responsive: true,
                        searching: false,
                        lengthChange: false,
                        pageLength: 3,
                        order: [
                            [0, "desc"]
                        ],
                        columnDefs: [
                            {
                                "targets": [ 0 ],
                                "visible": false
                            }
                        ],
                        language: {
                            url: "//cdn.datatables.net/plug-ins/1.10.19/i18n/Russian.json"
                        },
                        preDrawCallback: function(settings) {
                            $('[data-ed="adm-action"]').off('click');
                            $('[data-ed="ed-loader"]').css({
                                display: 'none'
                            });
                        },
                        drawCallback: function(settings) {
                            $('.ed-loader-table').css({
                                display: 'none'
                            });
                            $('#ed-task-table').css({
                                opacity: '1'
                            });
                            $('[data-ed="adm-action"]').on('click', function() {
                                console.log(123);
                                $this = $(this);
                                $id = $this.attr('data-tid');
                                if ($this.attr('data-upd') === 'status') {
                                    $.ajax({
                                        type: "POST",
                                        url: '/',
                                        data: {action: 'edit', taskId: $id},
                                        success: function(response) {
                                            console.log(333);
                                            console.log(response);
                                            console.log(222);
                                            if (response.status === 1) {
                                                $td = $this.closest('td');
                                                $this.off();
                                                $this.remove();
                                                $td.html('Выполнено');
                                                $('[data-ed="task"][data-target="'+$id+'"]').find('[data-ed="adm-action"][data-upd="content"]')[0].remove();
                                                $('[data-ed="task"][data-target="'+$id+'"]').css('background-color', '#78da786b');
                                                $('#ed-task-table').css({
                                                    opacity: 0
                                                });
                                                $('[data-ed="ed-loader"]').css({
                                                    display: 'flex'
                                                });
                                                buildTable(1);
                                            } else if (response.status === -1) {
                                                window.location.href = window.location.host+'/?action=login';
                                            }
                                        },
                                        error: function(response) {
                                            console.log(response);
                                        },
                                        dataType: 'json'
                                    });
                                } else if ($(this).attr('data-upd') === 'content') {
                                    $('[data-ed="taskdescedit"]').val(getText($(this).closest('td').find('span')));
                                    $('[data-ed="submtaskdescedit"]').off();
                                    $('[data-ed="submtaskdescedit"]').on('click', function() {
                                        $('.ed-m-err').css({
                                            display: 'none'
                                        });
                                        desc = $('[data-ed="taskdescedit"]').val();
                                        if (desc.length == 0) {
                                            $('.ed-m-err').css({
                                                display: 'flex'
                                            });
                                        } else {
                                            $('[data-ed="ed-loader"]').css({
                                                display: 'flex'
                                            });
                                            $.ajax({
                                                type: "POST",
                                                url: '/',
                                                data: {action: 'edit', taskId: $id, desc: desc},
                                                success: function(response) {
                                                    $('[data-ed="ed-loader"]').css({
                                                        display: 'none'
                                                    });
                                                    $('#taskEditModal').modal('toggle');
                                                    if (response.status === 1) {
                                                        $('[ data-ed="adm-action"][data-upd="content"][data-tid="'+$id+'"]').closest('td').find('span').text(desc);
                                                        $('#ed-task-table').css({
                                                            opacity: 0
                                                        });
                                                        $('[data-ed="ed-loader"]').css({
                                                            display: 'flex'
                                                        });
                                                        buildTable(1);
                                                    } else if (response.status === -1) {
                                                        window.location.href = window.location.host+'/?action=login';
                                                    }
                                                },
                                                dataType: 'json'
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
    }
    buildTable(0);

    function validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }

    $('[data-ed="submnewtask"]').on('click', function(e) {
        e.preventDefault();
        $('.ed-m-err').css({
            display: 'none'
        });
        uname = $('#task-user').val();
        email = $('#task-email').val();
        desc = $('#task-desc').val();
        ec = 0;
        if (uname.length < 2) {
            ec++;
            $('[data-ed="ed-m-err-name"]').css({
                display: 'flex'
            });
        }
        if (!validateEmail(email)) {
            ec++;
            $('[data-ed="ed-m-err-email"]').css({
                display: 'flex'
            });
        }
        if (desc.length < 2) {
            ec++;
            $('[data-ed="ed-m-err-desc"]').css({
                display: 'flex'
            });
        }

        if (ec === 0) {
            $('[data-ed="ed-loader"]').css({
                display: 'flex'
            });
            console.log('sending');
            $.ajax({
                type: "POST",
                url: 'index.php?action=add',
                data: {action: 'add', name: uname, email: email, desc: desc},
                success: function(response) {
                    console.log('send');
                    console.log(response);
                    $('[data-ed="ed-loader"]').css({
                        display: 'none'
                    });
                    $('#taskAddModal').modal('toggle');
                    if (response.status === 1) {
                        $('.ed-loader-table').css({
                            display: 'flex'
                        });
                        $('#ed-task-table').css({
                            opacity: '0'
                        });
                        alert('Задача добавлена успешно!');
                        $.ajax({
                            type: "POST",
                            url: 'index.php?action=get',
                            success: function(response) {
                                window.tableData = response;
                                buildTable(1);
                            }
                        });
                    }
                },
                error: function(e) {
                    console.log(e.responseText);
                },
                dataType: 'json'
            });
        }
    });
    //old
    /*
    $('[data-ed="submnewtask"]').on('click', function() {
        $('.ed-m-err').css({
            display: 'none'
        });
        id = $('#task-user').val();
        desc = $('[data-ed="taskdescadd"]').val();

        if (desc.length == 0) {
            $('.ed-m-err').css({
                display: 'flex'
            });
        } else {
            $('[data-ed="ed-loader"]').css({
                display: 'flex'
            });
            console.log('sending');
            $.ajax({
                type: "POST",
                url: 'index.php?action=add',
                data: {action: 'add', id: id, desc: desc},
                success: function(response) {
                    console.log('send');
                    console.log(response);
                    $('[data-ed="ed-loader"]').css({
                        display: 'none'
                    });
                    $('#taskAddModal').modal('toggle');
                    if (response.status === 1) {
                        $('.ed-loader-table').css({
                            display: 'flex'
                        });
                        $('#ed-task-table').css({
                            opacity: '0'
                        });
                        $.ajax({
                            type: "POST",
                            url: 'index.php?action=get',
                            success: function(response) {
                                window.tableData = response;
                                buildTable(1);
                            }
                        });
                    }
                },
                error: function(e) {
                    console.log(e.responseText);
                },
                dataType: 'json'
            });
        }
    });
    */
});