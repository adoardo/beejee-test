set names 'utf8';

CREATE TABLE IF NOT EXISTS `bj_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login`    varchar(100) NOT NULL UNIQUE,
  `email`    varchar(100) NOT NULL UNIQUE,
  `fio`        varchar(250) DEFAULT NULL,
  `password`    varchar(255) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `dstatus` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='utf8_general_ci';

-- паролям ставлю просто md5 без заморочек
INSERT INTO `bj_users` (`id`, `login`, `email`, `fio`, `password`, `status`) VALUES
(1, 'test1', 'test1@test.com', 'Имя1 Фамилия1', '5a105e8b9d40e1329780d62ea2265d8a', 0),
(2, 'test2', 'test2@test.com', 'Имя2 Фамилия2', 'ad0234829205b9033196ba818f7a872b', 0),
(3, 'test3', 'test3@test.com', 'Имя2 Фамилия2', '8ad8757baa8564dc136c1e07507f4a98', 0),
(4, 'admin', 'admin@admin.com', 'Админ Админов', '202cb962ac59075b964b07152d234b70', 1);

CREATE TABLE IF NOT EXISTS `bj_tasks` (
  `id` int(11) PRIMARY KEY AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `content` text NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` DATETIME DEFAULT   CURRENT_TIMESTAMP,
  `updated_at` DATETIME DEFAULT   CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='utf8_general_ci';

INSERT INTO bj_tasks(`user_id`, `content`, `status`) VALUES
(1, 'Тестовое описание задачи 1', 0),
(2, 'Тестовое описание задачи 2', 0),
(1, 'Тестовое описание задачи 3', 0),
(3, 'Тестовое описание задачи 4', 0),
(1, 'Тестовое описание задачи 5', 0);